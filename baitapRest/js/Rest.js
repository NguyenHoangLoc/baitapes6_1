let countAverage = (...rest) => {
  let sumScore = 0;
  let averageScore = 0;

  for (let i = 0; i < rest.length; i++) {
    sumScore += rest[i];
    averageScore = sumScore / rest.length;
  }
  return averageScore.toFixed(2);
};
let countScoreGrade1 = () => {
  let scoreMath = document.querySelector("#inpToan").value * 1;
  let scorePhys = document.querySelector("#inpLy").value * 1;
  let scoreChem = document.querySelector("#inpHoa").value * 1;
  let result = countAverage(scoreMath, scorePhys, scoreChem);

  document.querySelector("#tbKhoi1").innerHTML = result;
};

let countScoreGrade2 = () => {
  let scoreLite = document.querySelector("#inpVan").value * 1;
  let scoreHist = document.querySelector("#inpSu").value * 1;
  let scoreGeog = document.querySelector("#inpDia").value * 1;
  let scoreEng = document.querySelector("#inpEnglish").value * 1;
  let result = countAverage(scoreLite, scoreHist, scoreGeog, scoreEng);

  document.querySelector("#tbKhoi2").innerHTML = result;
};
